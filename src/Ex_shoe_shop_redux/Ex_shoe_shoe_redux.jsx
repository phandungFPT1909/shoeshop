import React, { Component } from 'react'
import { connect } from 'react-redux'
import CartShoe from './CartShoe'
import DetailShoe from './DetailShoe'
import ListShoe from './ListShoe'
import styles from "./styles.module.css"

 class Ex_shoe_shoe_redux extends Component {
  render() {
    let tongSoLuong = this.props.gioHang.reduce((tsl,item) => {
        return tsl += item.soLuong
    },0)
    return (
      <div className='container py-5'>
        <h3>Giỏ hàng redux</h3>
        <div className='pt-5'><span className={`${styles.fonSize}  text-danger`}>Giỏ hàng ({tongSoLuong})</span></div>
        <CartShoe />
        <ListShoe />
        <DetailShoe />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        gioHang: state.shoeReducer_ShoeShop.cart
    }
}
export default connect(mapStateToProps,null)(Ex_shoe_shoe_redux)