import { dataShoe } from "../../dataShoe";
import * as actionType from "../constants/shoeContant"

let initialState = {
    shoeArr:dataShoe,
    detail:dataShoe[0],
    cart: [],
};
export const shoeReducer_ShoeShop = (state=initialState,action) => {
    switch(action.type) {
        case actionType.XEM_CHI_TIET: {
            return {...state,detail:action.payload}
        }
        case actionType.THEM_GIO_HANG: {
            let newGioHang = [...state.cart];
            let index = newGioHang.findIndex((item) =>{
                return item.id == action.payload.id
            })
            if(index == -1) {
               let cartItem = {...action.payload,soLuong:1}
                newGioHang.push(cartItem)
            } else {
                newGioHang[index].soLuong += 1
            }
            return {...state,cart:newGioHang};
        }
        case actionType.XOA_ITEM_CART: {
            let newGioHang = [...state.cart];
            let index = newGioHang.findIndex((item) =>{
                return item.id == action.idSp
            })
            if(index !== -1) {
                newGioHang.splice(index,1)
            }
            return {...state,cart:newGioHang};
        }
        case actionType.TANG_GIAM_SO_LUONG: {
            let newGioHang = [...state.cart];
            let index = newGioHang.findIndex((item) =>{
                return item.id == action.idSp
            })
            if(action.tangGiam) {
                newGioHang[index].soLuong += 1
            } else {
                if(newGioHang[index].soLuong > 1) {
                    newGioHang[index].soLuong -= 1
                } else {
                    newGioHang.splice(index,1)
                }
            }
            return {...state,cart:newGioHang};
        }
    }
    return {...state}
}