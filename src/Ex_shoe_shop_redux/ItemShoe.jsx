import React, { Component } from "react";
import { connect } from 'react-redux';
import { chanDetailAction, chanAddToCartAction } from './redux/actions/shoeAction';

 class ItemShoe extends Component {
  render() {
    let { itemShoe } = this.props;
    return (
      <div className="col-3 mb-5">
        <div className="card h-100">
          <img className="card-img-top" src={itemShoe.image} alt="" />
          <div className="card-body">
            <h4 className="card-title">{itemShoe.name}</h4>
            <p className="card-text">{itemShoe.price}</p>
          </div>
          <div className="d-flex justify-content-around pb-3">
            <button onClick={() => {
                this.props.handleChangeDetail(itemShoe)
            }} className="btn btn-danger">Xem chi tiết</button>
            <button onClick={() => {
                this.props.handleAddToCart(itemShoe)
            }} className="btn btn-danger">Chọn mua</button>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
      handleChangeDetail:(shoe) => {
        dispatch(chanDetailAction(shoe))
      },
      handleAddToCart:(shoe) => {
        dispatch(chanAddToCartAction(shoe))
      }
    }
}

export default connect(null,mapDispatchToProps)(ItemShoe)