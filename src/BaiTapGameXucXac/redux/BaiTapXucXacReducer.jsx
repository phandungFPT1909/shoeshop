

const stateDefault = {
    taiXiu:"", 
    mangXucXac: [
        {ma:6,hinhAnh:"./img/imgXucSac/6.png"},
        {ma:6,hinhAnh:"./img/imgXucSac/6.png"},
        {ma:6,hinhAnh:"./img/imgXucSac/6.png"},
    ],
    banDa:0,
    soBanThang:0,
    tongSoBanChoi:0,
}

const BaiTapGameXucXacReducer = (state=stateDefault,action) => {
    switch(action.type) {
        case "DAT_CUOC": {
            state.taiXiu = action.payload;
            return {...state}
        }
    
        case "PLAY_GAME": {
            if(state.taiXiu == "") {
                alert("Bạn chưa đặt cược")
                return {...state}
           }

                // bƯớC 1 XỬ LÍ RANDOM XÚC XẮC
                let mangXucXacNgauNhien = [];
                for(let i = 0; i< 3;i++) {
                    // mỗi lần lặp random ra số ngẩu nhiên từ 1 -> 6
                    let soNgauNhien = Math.floor(Math.random() * 6) + 1;
                    // TẠO RA MỘT ĐỐI TƯỢNG XÚC XẮC
                    let xucXacNgauNhien = {ma:soNgauNhien,hinhAnh:`./img/imgXucSac/${soNgauNhien}.png`}
                    mangXucXacNgauNhien.push(xucXacNgauNhien)
                }
            
                state.mangXucXac = mangXucXacNgauNhien
                // xử lý tăng bàn chơi
                state.tongSoBanChoi += 1
                //   xử lý số bàn thắng
                let tongSoDiem = mangXucXacNgauNhien.reduce((tongDiem,xucXac,index) => {
                    return tongDiem += xucXac.ma
                },0)
                // xét điều kiện để người dùng thắng game
                if((tongSoDiem > 11 && state.taiXiu === 1) || (tongSoDiem < 11 && state.taiXiu === -1)) {
                    state.soBanThang +=1;
                    state.banDa = 1
                    // return {...state}
                } else {
                    state.banDa = -1
                    // return {...state}
                }
                return {...state}
            
        }

    

    
    
        default:
            return {...state}
    }
}
export default BaiTapGameXucXacReducer