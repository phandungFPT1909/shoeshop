import React, { Component } from 'react'
import "./BaiTapGameXucXac.css"
import ThongTinTroChoi from './ThongTinTroChoi'
import XucXac from './XucXac'
import { connect } from 'react-redux';
 class BaiTapGameXucXac extends Component {
  render() {
    return (
      <div className='game'>
        <div className='title-game text-center mt-5 display-4'>
            Game xúc xắc
        </div>
        <div className='row'>
            <div className='col-4'>
                <button onClick={() => { 
                    this.props.datCuoc(1)
                 }} className='btn btn-danger btnGame'>Tài</button>
            </div>
            <div className='col-4'>
                <XucXac />
            </div>
            <div className='col-4'>
            <button onClick={() => { 
                    this.props.datCuoc(-1)
                 }} className='btn btn-danger btnGame'>Xỉu</button>
            </div>
        </div>
          <div className='thongTinTroChoi text-center'>
                <ThongTinTroChoi />
                <button onClick={() => {
                    this.props.playGame()
                }} className='btn btn-success p-2 display-4 mt-3'>Play game</button>
            </div>
      </div>
    )
  }
}
const mapDispatchToProps = (dispatch) => {
    return {
        datCuoc:(check) => {
            const action = {
                type:"DAT_CUOC",
                payload:check
            }
            dispatch(action)
        },
        playGame:() => {
            dispatch({
                type:"PLAY_GAME"
            })
        }
    }
}
export default connect(null,mapDispatchToProps)(BaiTapGameXucXac)
