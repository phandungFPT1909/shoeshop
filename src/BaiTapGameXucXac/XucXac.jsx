import React, { Component } from 'react'
import { connect } from 'react-redux';

 class XucXac extends Component {
    renderXucXac =() => {
        return this.props.mangXucXac.map((item,index) => {
            return <img key={index} className='ml-2' style={{width:"80px",height:"80px"}} src={item.hinhAnh} />
        })
    }

  render() {
    return (
      <div>
       { this.renderXucXac()}
      </div>
    )
  }
}


const mapStateToProps = state => {
    return {
        mangXucXac:state.BaiTapGameXucXacReducer.mangXucXac
    }
}

export default connect(mapStateToProps,null)(XucXac)