import React, { Component } from 'react'
import { connect } from 'react-redux';

 class ThongTinTroChoi extends Component {
handleTaiXiu = () => {
  let result;
  if(this.props.taiXiu == 1) {
    result = "Bạn chọn Tài";
  } else if(this.props.taiXiu == -1) {
    result = "Bạn chọn Xỉu";
  } else {
    result = "Xin mời bạn chọn tài hoặc xỉu"
  }
  return result
}

handleThangThua = () => {
  let result;
  if(this.props.banDa == 1) {
    result = "Bạn thắng";
  } else if(this.props.banDa == -1) {
    result = "Bạn thua";
  } 
  return result
}


  render() {
    // this.props.taiXiu ? "Tài" : "Xỉu"
    return (
      <div>
        <div className='font-size-ThongTinTroChoi'> 
        <span className='text-warning'>{this.handleTaiXiu()}</span>
        </div>
        <div className='font-size-ThongTinTroChoi'>
          <span className='text-danger'>{this.handleThangThua()}</span>
        </div>
        <div className='font-size-ThongTinTroChoi'>Bàn thắng: 
        <span className='text-danger'> {this.props.soBanThang}</span>
        </div>
        <div className='font-size-ThongTinTroChoi'>Tổng số bàn chơi: 
        <span className='text-primary'> {this.props.tongSoBanChoi}</span>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    
    return {
        soBanThang:state.BaiTapGameXucXacReducer.soBanThang,
        taiXiu:state.BaiTapGameXucXacReducer.taiXiu,
        tongSoBanChoi:state.BaiTapGameXucXacReducer.tongSoBanChoi,
        banDa:state.BaiTapGameXucXacReducer.banDa
    }
}

export default connect(mapStateToProps,null)(ThongTinTroChoi)